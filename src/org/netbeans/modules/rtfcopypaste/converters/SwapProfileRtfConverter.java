package org.netbeans.modules.rtfcopypaste.converters;

import javax.swing.JEditorPane;
import org.netbeans.modules.rtfcopypaste.utils.ConverterSettings;
import org.netbeans.modules.rtfcopypaste.utils.EditorProfileManager;

public class SwapProfileRtfConverter extends RtfConverter {

    public static String HIGHLIGHTS = "HIGHTLIGHTS";
    public static String DEFAULT = "DEFAULT";
    
    @Override
    public String convertContentToRtf(JEditorPane pane) {

        RtfConverter rtfConverter = selectRtfConverter();

        String currentProfile = EditorProfileManager.getDefault().getCurrentFontAndColorsProfile();
        String selectedProfile =  ConverterSettings.getDefault().getSelectedProfile();

        if (currentProfile.equals(selectedProfile)) {
            return rtfConverter.convertContentToRtf(pane);
        } else {
            EditorProfileManager.getDefault().setCurrentFontAndColorProfile(selectedProfile);
            String rtftext = rtfConverter.convertContentToRtf(pane);
            EditorProfileManager.getDefault().setCurrentFontAndColorProfile(currentProfile);
            return rtftext;
        }
    }

    private RtfConverter selectRtfConverter() {
        if (ConverterSettings.getDefault().getSelectedConverter().equals(HIGHLIGHTS)) {
            return new HighlightsRtfConverter();
        } else {
            return new DefaultRtfConverter();
        }
    }
}
