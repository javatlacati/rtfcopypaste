package org.netbeans.modules.rtfcopypaste.action;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import javax.swing.AbstractAction;
import javax.swing.JEditorPane;
import org.netbeans.modules.rtfcopypaste.RTFTransferable;
import org.netbeans.modules.rtfcopypaste.converters.RtfConverter;
import org.netbeans.modules.rtfcopypaste.converters.SwapProfileRtfConverter;
import org.openide.cookies.EditorCookie;
import org.openide.util.Utilities;

public class RTFCopyPasteAction extends AbstractAction {

    private static final RTFCopyPasteAction INSTANCE = new RTFCopyPasteAction();

    public static RTFCopyPasteAction getDefault() {
        return INSTANCE;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        action();
    }

    private void action() {
        final EditorCookie editorCookie
                = Utilities.actionsGlobalContext().lookup(EditorCookie.class);
        if (editorCookie == null) {
            return;
        }
        for (final JEditorPane pane : editorCookie.getOpenedPanes()) {
            if (pane != null && pane.isShowing()) {
                String rtf = convertContentToRtf(pane);
                putRtfToClipboard(rtf);
                return;
            }
        }
    }

    private String convertContentToRtf(final JEditorPane pane) {
        RtfConverter converter = new SwapProfileRtfConverter();
        return converter.convertContentToRtf(pane);
    }

    private void putRtfToClipboard(String rtf) {
        ByteArrayInputStream bais = new ByteArrayInputStream(rtf.getBytes());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new RTFTransferable(bais), null);
    }
}
