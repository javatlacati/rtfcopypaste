package org.netbeans.modules.rtfcopypaste.utils;

import org.netbeans.modules.rtfcopypaste.converters.SwapProfileRtfConverter;
import org.netbeans.modules.rtfcopypaste.options.SpinnerFontModel;

public class ConverterSettings {

    private String selectedProfile;
    private Integer selectedFontSize;
    private String selectedConverter;
    
    private static final ConverterSettings INSTANCE = new ConverterSettings();

    public static ConverterSettings getDefault() {
        return INSTANCE;
    }
    
    public ConverterSettings() {
        selectedProfile = DefaultEditorProfileManager.getDefault().getCurrentFontAndColorsProfile();
        selectedFontSize = SpinnerFontModel.getDefaultValue();
        selectedConverter = SwapProfileRtfConverter.HIGHLIGHTS;
    }
    
    public String getSelectedProfile() {
        return selectedProfile;
    }

    public void setSelectedProfile(String profile) {
        selectedProfile = profile;
    }

    public Integer getSelectedFontSize() {
        return selectedFontSize;
    }

    public void setSelectedFontSize(Integer fontSize) {
        selectedFontSize = fontSize;
    }

    public String getSelectedConverter() {
        return selectedConverter;
    }

    public void setSelectedConverter(String converterDesc) {
        selectedConverter = converterDesc;
    }
}
