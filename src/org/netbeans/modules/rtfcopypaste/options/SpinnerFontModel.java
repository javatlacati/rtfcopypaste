package org.netbeans.modules.rtfcopypaste.options;

import javax.swing.SpinnerModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SpinnerFontModel implements SpinnerModel {

    private static final Integer DEFAULT_VALUE = 10;
    private static final Integer MAX_VALUE = 40;
    private static final Integer MIN_VALUE = 6;
    private Integer currValue = DEFAULT_VALUE;
    private ChangeListener listener;

    public static Integer getDefaultValue() {
        return DEFAULT_VALUE;
    }
    
    @Override
    public Object getValue() {
        return currValue;
    }

    @Override
    public void setValue(Object v) {
        Integer newValue = (Integer) v;
        if (!newValue.equals(currValue)) {
            currValue = newValue;
            notifyChange();
        }
    }

    @Override
    public Object getNextValue() {
        if (currValue >= MAX_VALUE) {
            return null;
        }
        return currValue + 1;
    }

    @Override
    public Object getPreviousValue() {
        if (currValue <= MIN_VALUE) {
            return null;
        }
        return currValue - 1;
    }

    private synchronized void notifyChange() {
        if (listener != null) {
            ChangeEvent changeEvent = new ChangeEvent(currValue);
            listener.stateChanged(changeEvent);
        }
    }

    @Override
    public synchronized void addChangeListener(ChangeListener l) {
        this.listener = l;
    }

    @Override
    public synchronized void removeChangeListener(ChangeListener l) {
        this.listener = null;
    }
}
